# Astronote Cahier des charges
---
### SUJET
- **But**: 	Développer une application mobile crossplatform de gestion des notes de frais d'interface à [**enDI**](https://framagit.org/endi/endi) ("**en**treprendre **DI**fféremment") 
- **N.B.**: 	Publiée sous license libre ([GPLv3 License](https://framagit.org/membres-astrolabe/astronote/-/blob/master/LICENSE.txt)) dans un magasin d'application libre. Développée avec Flutter.

##### Etapes de travail:
- **Etape 1**: 

Première version simplifiée permettant de créer une note de frais et de rentrer à minima la date, le type de dépense et les charges (montants HT, TTC, TVA)  
Stocker les informations en local dans une base de donnée simple (objets typés et des binaires)  
Envoie sécurisé des informations collectées vers enDI (des contributions à enDI seront peut-être nécessaires)  
- **Etape 2**:

Système de pièces jointes de justificatifs et amélioration du design graphique  
Inclure la prise de photo de la preuve de dépense et l'attacher en tant que pièce justificative  
Reconnaissance de caractères optique (OCR) avec remplissage automatique lors de la prise de photo de la pièce justificative  
Utilisation de briques logicielles disponibles en libre ou API standard Android permettant l'OCR  
- **Environment**: 	Android / Lineage OS, F-Droid / Play Store, logiciel libre, progiciel métier
---
## ETAPE 1
[ ] ==**CONNECTION**==

Scénario:
1.	Un utilisateur passif veut se connecter : il doit le faire via un URL spécifique et des identifiants crées et envoyés auparavant par un administrateur ou un membre de l'équipe d'appui
2.	L’utilisateur rempli le formulaire de connexion avec les informations nécessaires (adresse e-mail et mot de passe) et clique sur le bouton de connexion
3.	L’utilisateur peut consulter le site d’enDI via l’URL si nécessaire
4.	L’utilisateur peut consulter le site d’Astrolabe via l’URL si nécessaire

L’utilisateur se connecte automatiquement à son compte enDI via une API Rest  
Il est nécessaire de faire perdurer une session avec cookie  


[ ] ==**ACCUEIL**==

L'accueil permet d’assurer le suivi des notes enregistrées en Brouillon ou en attente de validation  
Les notes sont affichées dans un tableau qui permet à l’utilisateur de :
- suivre l’état de ses notes (enregistrées en Brouillon ou en attente de validation)
- voir les informations globales des notes (la date, les montants HT, TVA, TTC, le nombre de kilomètres)
- voir le total des montants HT, de TVA, TTC, nombre de kilomètres des notes
- consulter des notes spécifiques

Scenario:
1.	Une fois l’authentification réussie  
	Si l’utilisateur est enregistré sur une seule enseigne:		il est redirigé vers l’IHM de l’Accueil  
	Si l’utilisateur est enregistré sur plusieurs enseignes:	il est redirigé vers l’IHM de visualisation de choix de l’enseigne sur laquelle il souhaite intervenir puis vers l’IHM de l’Accueil  
2.	Les notes de dépenses enregistrées en Brouillon ou en attente de validation sont affichées  
3.	L’utilisateur peut consulter des notes de dépense spécifiques : si tel est le cas, il est renvoyé sur les IHM des notes qu’il souhaite consulter  
				
				
[ ] ==**GESTION D’UNE NOTE DE DÉPENSE**==

Les dépenses sont séparées en deux sections : les frais liés au fonctionnement de l’enseigne (Frais) et les frais liés directement à l’activité auprès des clients (Achats)  
Elles comprennent : 
- les dépenses Frais et Achats : la date, le type de dépense, la description, le montant HT, le montant TVA, le total TTC
- les dépenses kilométriques : la date, le type, la prestation, le départ, l’arrivée, le nombre de kilomètres à indemniser  

Seules les dépenses liées au fonctionnement de l'enseigne sont développées/gérées dans cette première étape  

Lorsque l'utilisateur sélectionne une note de dépense spécifique il est redirigé vers l'IHM de la note  
Cela permet d'assurer le suivi de : 
- l’état de la note de dépense
- les justificatifs présents ou non
- les commentaires enregistrés (incluant : son contenu, le nom de son auteur et sa date d’envoi)
- les dépenses liées à la note  

Scénario:
1. Un utilisateur actif veut consulter une note de dépenses spécifique : il se rend sur l'IHM de l'Accueil et sélectionne la note de dépense qu’il souhaite consulter
2. Le système affiche l’ensemble des informations de la note de dépense


[ ] ==**CRÉER UNE NOTES DE DÉPENSES**==

Scénario:
1. 	Un utilisateur actif veut créer une note de dépenses : il se rend sur l’IHM de l'Accueil et clique le bouton d’ajout
2.	L’utilisateur doit préciser le mois et l’année (le mois et l'année de la date du jour sont proposés par défaut)
3.	L’utilisateur peut choisir d’annuler ou valider son action  

Une note de dépense (Expense) est rattachée à une feuille de note de dépense (ExpenseSheet) qui est rattachée à un compte utilisateur (User) et une enseigne (Company)  
Il faut donc créer une note de dépense dans une feuille de note de dépense qui doit être créée si elle n'existe pas encore  
Une fois la note créée, elle est automatiquement enregistrée en tant que Brouillon  


[ ] ==**AJOUTER UNE DÉPENSE DE FRAIS (fonctionnement de l'enseigne)**==

Scénario:
1.	Un utilisateur actif veut ajouter une dépense de frais : il se rend sur l’IHM de l'Accueil, sélectionne la note dans laquelle il souhaite ajouter une dépense -> il est redirigé sur l’IHM de la note spécifique puis clique sur le bouton d’ajout
2.	L’utilisateur peut remplir un formulaire de frais généraux avec les informations nécessaires
3.	L’utilisateur peut remplir un formulaire de frais téléphoniques avec les informations nécessaires
4.	L’utilisateur peut choisir d’enregistrer ou d’annuler les dépenses ajoutées  

- les informations pour le formulaire de frais généraux sont :  la date*, la catégorie, le type de frais*, la description, le montant HT* et le montant TVA*
- Les informations pour le formulaire de frais téléphoniques sont : la date*, le type de frais*, le montant HT* et le montant TVA*  

(* = Les informations obligatoires pour la validation des formulaires)  

Si plusieurs taux de TVA doivent être renseignés alors le montant total doit être précisé  
Si la TVA n’est pas déductible : la valeur TTC doit être renseignée dans le montant HT et mettre 0 dans le montant TVA  
Ces dépenses peut être ajoutées seulement si la note est enregistrée en Brouillon  


[ ] ==**MODIFIER UNE DÉPENSE DE FRAIS (fonctionnement de l'enseigne)**==

Scénario:
1.	Un utilisateur actif veut modifier une dépense de frais : il se rend sur l’IHM de l'Accueil, sélectionne la note dans laquelle il souhaite modifier une dépense -> il est redirigé sur l’IHM de la note spécifique puis clique sur le bouton de modification
2.	L’utilisateur peut remplir un formulaire de frais généraux (prérempli des informations courantes) avec les informations nécessaires
3.	L’utilisateur peut remplir un formulaire de frais téléphoniques (prérempli des informations courantes) avec les informations nécessaires
4.	L’utilisateur peut choisir d’enregistrer ou d’annuler la dépense modifiée

- les informations pour le formulaire de frais généraux sont :  la date*, la catégorie, le type de frais*, la description, le montant HT* et le montant TVA*
- Les informations pour le formulaire de frais téléphoniques sont : la date*, le type de frais*, le montant HT* et le montant TVA*  

(* = Les informations obligatoires pour la validation des formulaires)  

Si plusieurs taux de TVA doivent être renseignés alors le montant total doit être précisé  
Si la TVA n’est pas déductible : la valeur TTC doit être renseignée dans le montant HT et mettre 0 dans le montant TVA  
Ces dépenses peuvent être modifiées seulement si la note est enregistrée en Brouillon   


[ ] ==**AJOUTER UNE DÉPENSE KILOMÉTRIQUE (fonctionnement de l'enseigne)**==

Scénario:
1.	Un utilisateur actif veut ajouter une dépense kilométrique : il se rend sur l’IHM de l'Accueil, sélectionne la note dans laquelle il souhaite ajouter une dépense -> il est redirigé sur l’IHM de la note spécifique puis clique sur le bouton d'ajout
2.	L’utilisateur rempli un formulaire de dépense kilométrique avec les informations nécessaires
3.	L’utilisateur peut choisir d’enregistrer ou d’annuler la dépense

- les informations pour le formulaire sont : la date*, la catégorie, le type de frais*, le point de départ*, le point d’arrivée*, la prestation, le nombre de kilomètres*  

(* = Les informations obligatoires pour la validation du formulaire)  

Cette dépense peut être ajoutée seulement si la note est enregistrée en Brouillon  


[ ] ==**MODIFIER UNE DÉPENSE KILOMÉTRIQUE (fonctionnement de l'enseigne)**==

Scénario:
1.	Un utilisateur actif veut modifier une dépense kilométrique : il se rend sur l’IHM de l'Accueil, sélectionne la note dans laquelle il souhaite modifier la dépense -> il est redirigé sur l’IHM de la note spécifique puis clique sur le bouton de modification
2.	L’utilisateur peut remplir un formulaire de dépense kilométrique (prérempli des informations courantes) avec les informations nécessaires
3.	L’utilisateur peut choisir d’enregistrer ou d’annuler la dépense modifiée.

- les informations pour le formulaire sont : la date*, la catégorie, le type de frais*, le point de départ*, le point d’arrivée*, la prestation, le nombre de kilomètres*  

(* = Les informations obligatoires pour la validation du formulaire)  

Cette dépense peut être modifiée seulement si la note est enregistrée en Brouillon  


[ ] ==**SUPPRIMER UNE DÉPENSE**==

Scénario:
1.  Un utilisateur actif veut supprimer une dépense : il se rend sur l’IHM de l'Accueil, sélectionne la note dans laquelle il souhaite supprimer une dépense -> il est redirigé sur l’IHM de la note spécifique et clique sur le bouton de suppression  
2.	Le système ouvre une pop-up pour s’assurer que l’utilisateur souhaite supprimer la dépense  
3.	L’utilisateur peut choisir de valider ou d’annuler la suppression  

Une dépense peut être supprimée seulement si la note de dépenses est enregistrée en Brouillon  


[ ] ==**ENREGISTRER UNE NOTE DE DÉPENSE**==

Scénario:
1.	Un utilisateur actif souhaite enregistrer une note de dépenses : il se rend sur l’IHM de l'Accueil, sélectionne la note sur laquelle il souhaite faire l’enregistrement -> il est redirigé sur l’IHM de la note spécifique et clique sur le bouton d'enregistrement
2.	L’utilisateur remplis un formulaire avec les informations nécessaires (les commentaires ne sont pas obligatoires)
3.	L’utilisateur peut choisir de valider ou d’annuler son action  

Seules les note de dépense à l'état Brouillon peuvent être enregistrées  
Les commentaires seront accessibles via l'IHM de l'Accueil une fois enregistrés  


[ ] ==**SUPPRIMER UNE NOTE DE DÉPENSE**==

Scénario:
1.	Un utilisateur actif veut supprimer une note de dépense : il se rend sur l’IHM de l'Accueil, sélectionne la note qu'il souhaite supprimer -> il est redirigé sur l’IHM de la note spécifique puis clique sur le bouton de suppression
2.	Le système ouvre une pop-up pour s’assurer que l’utilisateur souhaite bien supprimer la note
3.	L’utilisateur peut choisir de valider ou annuler la suppression

Seules les note de dépenses à l'état Brouillon peuvent être supprimées  
Si la note est en demande de validation, l’utilisateur peut la réenregistrer en tant que Brouillon  
Cette action annulera automatiquement la demande de validation et passera la note à l’état de Brouillon  


[ ] ==**DEMANDER LA VALIDATION D’UNE NOTE DE DÉPENSE**==

Scénario:
1.	Un utilisateur actif veut demander la validation d’une note de dépense : il se rend sur l’IHM de l'Accueil, sélectionne la note dans laquelle il souhaite demander la validation -> il est redirigé sur l’IHM de la note spécifique puis clique sur le bouton de demande
2.	L’utilisateur rempli un formulaire de demande de validation (les commentaires ne sont pas obligatoires)
3.	L’utilisateur peut choisir de valider ou d’annuler sa demande

L’utilisateur ne peut pas valider ou invalider sa demande : seul l’administrateur applique ces actions  
Les notes de dépenses possèdent d’autres états mais ils ne sont pas gérés dans l’application  

----------

## ETAPE 2
[ ] ==**JOINDRE UN JUSTIFICATIF A UNE NOTE DE DÉPENSE**==

Scénario:
1.	Un utilisateur actif veut ajouter un justificatif à une note de dépense : il se rend sur l’IHM de l'Accueil, sélectionne la note dans laquelle il souhaite ajouter un justificatif -> il est redirigé sur l’IHM de la note spécifique, va dans la zone des justificatifs puis clique sur le bouton d’ajout
2.	L’utilisateur rempli un formulaire avec les informations nécessaires  
3.	L’utilisateur peut choisir de valider ou annuler son action

- les informations nécessaires pour le formulaire de justificatif sont : le type du document*, le choix du fichier (en local)* et sa description*    

(* = Les informations obligatoires pour la validation du formulaire)  

La description par défaut est le nom du fichier    
Un justificatif peut être joint peu importe l’état de la note de dépense  


[ ] ==**SUPPRIMER UN JUSTIFICATIF D’UNE NOTE DE DÉPENSE**==

Scénario:
1.	Un utilisateur actif veut supprimer un justificatif d’une note de dépense : il se rend sur l’IHM de l'Accueil, sélectionne la note dans laquelle il souhaite supprimer un justificatif -> il est redirigé sur l’IHM de la note spécifique, va dans la zone des justificatifs, sélectionne le fichier et clique sur le bouton de suppression
2.	Le système affiche une fenêtre avec les informations du fichier (sa description, son nom, sa taille, la date de son dépôt et la date de sa dernière modification) pour s’assurer que l’utilisateur souhaite bien supprimer le justificatif
3.	L’utilisateur peut choisir de valider ou d’annuler la suppression  

Un justificatif peut être supprimé peu importe l’état de la note de dépense  


[ ] ==**MODIFIER UN JUSTIFICATIF D’UNE NOTE DE DÉPENSE**==

Scénario:
1.	Un utilisateur actif veut modifier un justificatif d’une note de dépense : il se rend sur l’IHM de l'Accueil, sélectionne la note dans laquelle il souhaite modifier un justificatif -> il est redirigé sur l’IHM de la note spécifique, va dans la zone des justificatifs, sélectionne le fichier et clique sur le bouton de modification
2.	Le système affiche une fenêtre avec les informations du fichier (sa description, son nom, sa taille, la date de son dépôt et la date de sa dernière modification)
3.	L’utilisateur rempli un formulaire (prérempli des informations courantes, dont le fichier) avec les informations nécessaires
4.	L’utilisateur peut choisir de valider ou non sa modification

- les informations nécessaires pour le formulaire de justificatif sont : le type du document*, le choix du fichier (en local)* et sa description*    

(* = Les informations obligatoires pour la validation du formulaire)    

La description par défaut est le nom du fichier  
Un justificatif peut être modifié peu importe l’état de la note de dépense  


[ ] ==**GÉNÉRER UNE DÉPENSE GRÂCE A L'OCR**==

Scénario:
1.	Un utilisateur actif veut générer une dépense grâce à l'OCR : il se rend sur l’IHM de l'Accueil et selectionne la note dans laquelle il souhaite générer une dépense -> il est redirigé sur l’IHM de la note spécifique et clique sur le bouton de génération
2.	L’utilisateur peut choisir de générer une dépense à partir d’une photo existante ou de prendre une photo directement
3.	Le système OCR analyse les données à partir de la photo et calcule automatiquement les taux
4.	Le système ouvre un formulaire prérempli des informations courantes
5.	L’utilisateur peut choisir de modifier le formulaire, valider ou annuler


[ ] ==**AJOUTER UNE DÉPENSE D’ACHATS CONCERNANT L’ACTIVITÉ AUPRÈS DES CLIENTS**==

Scénario:
1.	Un utilisateur actif veut ajouter une dépense liées au client : il se rend sur l’IHM de l'Accueil, sélectionne la note dans laquelle il souhaite ajouter une dépense -> il est redirigé sur l’IHM de la note spécifique puis clique sur le bouton d’ajout
2.	L’utilisateur peut remplir un formulaire de frais généraux avec les informations nécessaires 
3.	L’utilisateur peut remplir un formulaire de frais téléphoniques avec les informations nécessaires
4.	L’utilisateur peut choisir d’enregistrer ou d’annuler la dépense

- les informations pour le formulaire de frais généraux sont :  la date*, la catégorie, le type de frais*, la description, le montant HT* et le montant TVA*, le client concerné  
- Les informations pour le formulaire de frais téléphoniques sont : la date*, le type de frais*, le montant HT* et le montant TVA*, le client concerné  

(* = Les informations obligatoires pour la validation des formulaires)  

Si plusieurs taux de TVA doivent être renseignés alors le montant total doit être précisé  
Si la TVA n’est pas déductible : la valeur TTC doit être renseignée dans le montant HT et mettre 0 dans le montant TVA  
Ces dépense peuvent être ajoutées seulement si la note est enregistrée en Brouillon 


[ ] ==**MODIFIER UNE DÉPENSE D’ACHATS CONCERNANT L’ACTIVITÉ AUPRÈS DES CLIENTS**==

Scénario:
1.	Un utilisateur actif veut modifier une dépense liées au client : il se rend sur l’IHM de l'Accueil, sélectionne la note dans laquelle il souhaite ajouter une dépense -> il est redirigé sur l’IHM de la note spécifique puis clique sur le bouton de modification
2.	L’utilisateur peut remplir un formulaire de frais généraux (prérempli des informations courantes) avec les informations nécessaires
3.	L’utilisateur peut remplir un formulaire de frais téléphoniques (prérempli des informations courantes) avec les informations nécessaires
4.	L’utilisateur peut choisir d’enregistrer ou d’annuler la dépense modifiée

- les informations pour le formulaire de frais généraux sont :  la date*, la catégorie, le type de frais*, la description, le montant HT* et le montant TVA*, le client concerné  
- Les informations pour le formulaire de frais téléphoniques sont : la date*, le type de frais*, le montant HT* et le montant TVA*, le client concerné  

(* = Les informations obligatoires pour la validation des formulaires)  

Si plusieurs taux de TVA doivent être renseignés alors le montant total doit être précisé  
Si la TVA n’est pas déductible : la valeur TTC doit être renseignée dans le montant HT et mettre 0 dans le montant TVA  
Ces dépenses peuvent être modifiées seulement si la note est enregistrée en Brouillon 


[ ] ==**AJOUTER UNE DÉPENSE KILOMÉTRIQUE CONCERNANT L’ACTIVITÉ AUPRÈS DES CLIENTS**==

Scénario:
1.	Un utilisateur actif veutajouter une dépense kilométrique : il se rend sur l’IHM de l'Accueil, sélectionne la note dans laquelle il souhaite modifier une dépense -> il est redirigé sur l’IHM de la note spécifique puis clique sur le bouton d'ajout
2.	L’utilisateur rempli un formulaire de dépense kilométrique avec les informations nécessaires
3.	L’utilisateur peut choisir d’enregistrer ou d’annuler la dépense

- les informations pour le formulaire sont : la date*, la catégorie, le type de frais*, le point de départ*, le point d’arrivée*, la prestation, le nombre de kilomètres*, le client concernée  

(* = Les informations obligatoires pour la validation du formulaire)  

Cette dépense peut être ajoutée seulement si la note est enregistrée en Brouillon 


[ ] ==**MODIFIER UNE DÉPENSE KILOMÉTRIQUE CONCERNANT L’ACTIVITÉ AUPRÈS DES CLIENTS**==

Scénario:
1.	Un utilisateur actif veut modifier une dépense kilométrique liée au client : il se rend sur l’IHM de l'Accueil, sélectionne la note dans laquelle il souhaite modifier une dépense -> il est redirigé sur l’IHM de la note spécifique puis clique sur le bouton de modification
2.	L’utilisateur peut remplir un formulaire de dépense kilométrique (prérempli des informations courantes) avec les informations nécessaires
3.	L’utilisateur peut choisir d’enregistrer ou d’annuler la dépense modifiée.

- les informations pour le formulaire sont : la date*, la catégorie, le type de frais*, le point de départ*, le point d’arrivée*, la prestation, le nombre de kilomètres*, le client concernée  

(* = Les informations obligatoires pour la validation du formulaire)  

Cette dépense peut être modifiée seulement si la note est enregistrée en Brouillon 


[ ] ==**DUPLIQUER UNE DÉPENSE**==

Scénario:
1.	Un utilisateur actif veut dupliquer une dépense : il se rend sur l’IHM de l'Accueil, sélectionne la note dans laquelle il souhaite dupliquer une dépense -> il est redirigé sur l’IHM de la note spécifique et clique sur le bouton pour dupliquer
2.	L’utilisateur précise dans un formulaire dans quelle note de dépenses la dépense doit être dupliquée : seules les notes de dépenses enregistrées en Brouillon peuvent être sélectionnées (la note courante est sélectionnée par défaut)
3.	L’utilisateur peut choisir de valider ou d’annuler la duplication

Lorsque l’utilisateur précise dans quelle note de dépenses la dépense doit être dupliquée, les montants HT et TVA de la dépense sont renseignés 
Une dépense peut être dupliquée seulement si la note de dépense est enregistrée en Brouillon  


[ ] ==**DUPLIQUER UNE NOTE DE DÉPENSE**==

Scénario:
1.	Un utilisateur actif souhaite dupliquer une note de dépense : il se rend sur l’IHM de l'Accueil, sélectionne la note qu'il souhaite dupliquer puis clique sur le bouton de duplication
2.	L’utilisateur précise le mois et l’année
3.	L’utilisateur peut choisir de valider ou d’annuler la duplication

Une note de dépense peut être dupliquée peu importe son état et la copie est automatiquement enregistrée en Brouillon  
