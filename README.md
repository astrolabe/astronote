# Astronote-FR  
Une application mobile (cross-platform) de gestion de notes de frais d'interface à [enDI](https://framagit.org/endi/endi) ; développée par Astrolabe avec [Flutter](https://flutter.dev/)  
 

## License

Astronote est une application libre (voir [LICENSE.txt](https://framagit.org/membres-astrolabe/astronote/-/blob/master/LICENSE.txt))  

----  

# Astronote-EN  

Expense report management mobile app (cross-platform) including communication with [enDI](https://framagit.org/endi/endi) ; developed by Astrolabe with [Flutter](https://flutter.dev/)  
 

## License

Published under free software license (read [LICENSE.txt](https://framagit.org/membres-astrolabe/astronote/-/blob/master/LICENSE.txt))
