import 'package:flutter/material.dart';

ThemeData appTheme() {
  return ThemeData(
    scaffoldBackgroundColor: Colors.white,
    fontFamily: 'Varela Round',
    textTheme: TextTheme(
        bodyText1: TextStyle(fontSize: 16.0, color: Colors.black),
        button: TextStyle(
            fontSize: 17.0, color: Colors.white, fontWeight: FontWeight.bold),
        headline1: TextStyle(fontSize: 26.0, color: Colors.black),
        headline2: TextStyle(fontSize: 21.0, color: Colors.black)),
    buttonTheme: ButtonThemeData(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
      buttonColor: Colors.indigo[900],
    ),
  );
}
