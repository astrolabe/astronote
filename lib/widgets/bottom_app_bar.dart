import 'package:flutter/material.dart';

class BottomAppBar extends StatefulWidget {
  @override
  _BottomAppBarState createState() => _BottomAppBarState();
}

class _BottomAppBarState extends State<BottomAppBar> {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Accueil'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.add),
          title: Text('Notes'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.business),
          title: Text('Enseignes'),
        ),
      ],

      selectedItemColor: Colors.indigo[900],

    );
  }
}
