import 'package:flutter/material.dart';
import 'package:astronote_app/models/company.dart';

class CompanyChoiceView extends StatefulWidget {
  @override
  _CompanyChoiceViewState createState() => _CompanyChoiceViewState();
}

class _CompanyChoiceViewState extends State<CompanyChoiceView> {
  Companie companies = new Companie();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.only(
            top: MediaQuery.of(context).size.height * 0.09,
            bottom: MediaQuery.of(context).size.height * 0.05),
        padding: EdgeInsets.only(
            left: MediaQuery.of(context).size.height * 0.05,
            right: MediaQuery.of(context).size.height * 0.05),
        child: Column(
          children: <Widget>[
            Text("Bonjour !", style: Theme.of(context).textTheme.headline1),
            SizedBox(height: MediaQuery.of(context).size.height * 0.08),
            Text("Pour quelle enseigne souhaitez-vous gérer vos notes de dépenses ?", style: Theme.of(context).textTheme.headline2,textAlign: TextAlign.center),
            SizedBox(height: MediaQuery.of(context).size.height * 0.07),
            ButtonTheme.fromButtonThemeData(
              data: Theme.of(context).buttonTheme,
              child: RaisedButton(
                elevation: 3.0,
                onPressed: () {
                  Navigator.pushNamed(context, '/dashboardScreen');
                },

                child: Text("C'est parti !",
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.button),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
