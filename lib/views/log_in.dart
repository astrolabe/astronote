import 'file:///D:/AndroidStudioProjects/astronote_app/lib/services/log_in_services.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class LogInView extends StatefulWidget {
//  static const routeName = '/logInScreen';

  @override
  _LogInViewState createState() => _LogInViewState();
}

class _LogInViewState extends State<LogInView> {

  var _obscurePassword = true;
  var _isLoading = false;

  final urlEndi = "https://endi.coop";
  final urlAstrolabe = "https://astrolabe.coop";

  final GlobalKey<FormState> _formKey = GlobalKey();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final TextEditingController urlController = new TextEditingController();
  final TextEditingController loginController = new TextEditingController();
  final TextEditingController passwordController = new TextEditingController();

  Future<void> _submit() async {
    if (!_formKey.currentState.validate()) {
      // Invalid!
      return;
    }
    _formKey.currentState.save();

    try {
      setState(() {
        _isLoading = true;
      });
      await Provider.of<LogIn>(context, listen: false).logIn(
          urlController.text, loginController.text, passwordController.text);
      if(Provider.of<LogIn>(context, listen: false).isLog){
        Navigator.pushNamed(context, '/companyChoiceScreen');
      }
    } catch (Exception) {
      setState(() {
        _isLoading = false;
      });
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        backgroundColor: Colors.red[300],
        duration: Duration(seconds: 3),
        content: Row(
          children: <Widget>[
            Icon(Icons.error_outline),
            Text('  Les données ne sont pas correctes'),
          ],
        ),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    final TextStyle style = Theme.of(context).textTheme.bodyText1;

    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(
            top: MediaQuery.of(context).size.height * 0.04,
            left: MediaQuery.of(context).size.height * 0.07,
            right: MediaQuery.of(context).size.height * 0.07),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Image.asset(
                "assets/astrolabe_logo.jpg",
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.01),
              Text("AstroNotes", style: style.copyWith(fontSize: 36.0)),
              SizedBox(height: MediaQuery.of(context).size.height * 0.08),
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      controller: urlController,
                      style: style,
                      keyboardType: TextInputType.url,
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.fromLTRB(20.0, 15.0, 0.0, 15.0),
                          labelText: "Lien enDI",
                          suffixIcon:
                              Icon(Icons.link, color: Colors.indigo[900]),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0))),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                    TextFormField(
                      controller: loginController,
                      style: style,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.fromLTRB(20.0, 15.0, 0.0, 15.0),
                          labelText: "E-mail",
                          suffixIcon:
                              Icon(Icons.mail, color: Colors.indigo[900]),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0))),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height * 0.03),
                    TextFormField(
                      controller: passwordController,
                      style: style,
                      obscureText: _obscurePassword,
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.fromLTRB(20.0, 15.0, 0.0, 15.0),
                          labelText: "Mot de passe",
                          suffixIcon: IconButton(
                              icon: Icon(
                                  _obscurePassword
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: Colors.indigo[900]),
                              onPressed: () {
                                setState(
                                    () => _obscurePassword = !_obscurePassword);
                              }),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0))),
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height * 0.07),
                    ButtonTheme.fromButtonThemeData(
                      data: Theme.of(context).buttonTheme,
                      child: RaisedButton(
                        elevation: 3.0,
                        onPressed: () {
                          _submit();
                        },
                        child: _isLoading
                            ? CircularProgressIndicator()
                            : Text("Connexion",
                                textAlign: TextAlign.center,
                                style: Theme.of(context).textTheme.button),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.08),
              InkWell(
                onTap: () async {
                  if (await canLaunch(urlEndi)) {
                    await launch(urlEndi);
                  }
                },
                child: Text(
                  "En collaboration avec enDI",
                  style: style.copyWith(
                      fontSize: 13.0, decoration: TextDecoration.underline),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.01),
              InkWell(
                onTap: () async {
                  if (await canLaunch(urlAstrolabe)) {
                    await launch(urlAstrolabe);
                  }
                },
                child: Text(
                  "Développé par Astrolabe",
                  style: style.copyWith(
                      fontSize: 13, decoration: TextDecoration.underline),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
