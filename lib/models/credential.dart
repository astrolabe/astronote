import 'package:flutter/foundation.dart';

class Credential {
  int userId;
  String url;
  String login;
  String password;

  Credential({this.url,this.login,this.password,this.userId});

  @override
  String toString() {
    return 'UserLogIn{url: $url, login: $login}';
  }
}