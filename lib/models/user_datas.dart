import 'package:astronote_app/models/company.dart';

class UserDatas {
  String lastname;
  List<Companie> companies;
  String firstname;
  String civilite;

  UserDatas({this.lastname, this.companies, this.firstname, this.civilite});


  UserDatas.fromJson(Map<String, dynamic> json) {
    lastname = json['lastname'];
    if (json['companies'] != null) {
      companies = new List<Companie>();
      json['companies'].forEach((v) {
        companies.add(new Companie.fromJson(v));
      });
    }
    firstname = json['firstname'];
    civilite = json['civilite'];
  }

//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['lastname'] = this.lastname;
//    if (this.companies != null) {
//      data['companies'] = this.companies.map((v) => v.toJson()).toList();
//    }
//    data['firstname'] = this.firstname;
//    data['civilite'] = this.civilite;
//    return data;
//  }



  @override
  String toString() {
    return 'User{lastname: $lastname, companies: $companies, firstname: $firstname, civilite: $civilite}';
  }
}