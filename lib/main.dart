import 'file:///D:/AndroidStudioProjects/astronote_app/lib/services/log_in_services.dart';
import 'package:astronote_app/themes/style.dart';
import 'package:astronote_app/views/company_choice.dart';
import 'package:astronote_app/views/dashboard.dart';
import 'package:astronote_app/views/log_in.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: LogIn()),
       ],
      child: Consumer<LogIn>(
        builder:(context, logIn, _) => MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: appTheme(),
            home: logIn.isLog ? CompanyChoiceView() : LogInView(),
            routes: {
              "/logInScreen": (context) => LogInView(),
              "/companyChoiceScreen": (context) => CompanyChoiceView(),
              "/dashboardScreen": (context) => DashBoardView()
            },
          ),
      ),
    );
  }
}
