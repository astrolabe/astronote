# Astronote Specification
---
### LEXICON
- **EAS** : **E**nterprise **A**pplication **S**oftware
- **HCI** :	**H**uman-**C**omputer **I**nterface
- **OCR** : **O**ptical **C**haracter **R**ecognition
- **VAT** :	**V**alue-**A**dded **T**ax

### SPECIFICATION
- **Goal**: 	Develop expense report management mobile app crossplatform including communication with [**enDI**](https://framagit.org/endi/endi) ("**en**treprendre **DI**fféremment") EAS 
- **Note**: 	Published under free software license ([GPLv3 License](https://framagit.org/membres-astrolabe/astronote/-/blob/master/LICENSE.txt)) on an open-source app store

##### Work packages:
- **Step 1**: 

Prototype allowing input of expense date, type and charge (incl. and excl. taxes)  
Using local data (typed objects and binaries)  
Secured data upload to enDI EAS (enDI itself might need some work to ensure compatibility)
- **Step 2**:

File attachment system and overhauled user interface  
Using uploaded snapshot of expense document as supporting document  
OCR-based data input autocompletion based on said snapshot  
Using OCR software components from open-source repository or Android standard API 
- **Environment**: 	Android / Lineage operating systems, F-Droid / Play Store app stores, free software, EAS
---
## STEP 1
[ ] ==**CONNECTION**==

Baseline scenario:
1.	Passive user logs in using custom URL and logs sent by admin or support team
2.	User inputs username (i.e. e-mail address) and password in login form then submits
3.	If necessary, user can visit enDI official website using URL
4.	If necessary, user can visit Astrolabe official website using URL

Connection to enDI is automated with REST API  
Sessions are cookie-based


[ ] ==**DASHBOARD**==

Dashboard ensures draft and unvalidated expense reports management  
Every expense report in the dashboard features the following parameters:
- Expense status (validated, unvalidated or draft)
- Expense data (date, charge incl. and excl. taxes, VAT, distance)
- Sum data (total incl. and excl. taxes, total VAT, total distance)
- Consult details of specific expense reports

Baseline scenario:
1.	Authentication succeeds
	If user is associated with a single company:		User is redirected to company dashboard HCI
	If user is associated with more than one company:	User is redirected to company selection HCI, then relevant company dashboard HCI
2.	Draft and unvalidated expense reports are displayed
3.	User can view details of specific expense reports, in which case he is redirected to specific HCI
				
				
[ ] ==**MANAGE EXPENSE REPORT**==

Expenses are divided into two sections : costs related to a company (Costs) and the costs related to a customer (Purchases)   
Expenses include :
- date, type, description, charge incl. and excl. taxes, VAT
- for mileage expense : date, type, provision, origin, destination, distance  

Only expenses report related to a company will be developed in this step  

Expense report dashboard allows user to keep track of:
- Expense report status
- Supporting document (or lack thereof)
- Saved comments (including writer name, writing date and comment content)
- Business expenses


Baseline scenario:
1. Active user goes to HCI and picks specific expence report
2. All expense report details are displayed


[ ] ==**CREATE EXPENSE REPORT**==

Baseline scenario:
1. 	Active user goes to HCI and clicks expense report creation button
2.	User inputs date (month and year), with current date as default
3.	User confirms or cancels operation

Expense reports ("Expense") are associated with expense sheets ("ExpenseSheet") which themselves are associated with user account ("User") and company ("Company")  
Expense reports are created as parts of an expense sheet, which need to be created first  
Newly created expense reports are automatically saved as drafts


[ ] ==**ADD BUSINESS-RELATED EXPENSE (company)**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to add to. User is redirected to specific expense report HCI and clicks add button
2.	User can complete overhead form with appropriate data 
3.	User can complete line charge form with appropriate data
4.	User confirms or cancels expense addition

- Overhead form data to be completed: date, group, type*, description, charge incl. and excl. taxes*
- Line charge form data to be completed: date*, type*, charge incl. and excl. taxes*

(* = required data for form validation)  

If multiple VAT rates need to be provided, total sum should be input  
If VAT is not deductible, charge incl. taxes should be input in "charge excl. taxes" field and 0 should be input in "VAT" field  
Expense can only be added if expense report is already saved as draft


[ ] ==**EDIT BUSINESS-RELATED EXPENSE (company)**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to edit from. User is redirected to specific expense report HCI and clicks specific edit button
2.	User can complete overhead form with appropriate data (fields have current values as default values)
3.	User can complete line charge form with appropriate data (fields have current values as default values)
4.	User confirms or cancels expense edition

- Overhead form data to be completed: date, category, type*, description, charge incl. and excl. taxes*
- Line charge form data to be completed: date*, type*, charge incl. and excl. taxes*  

(* = required data for form validation)  

If multiple VAT rates need to be provided, total sum should be input  
If VAT is not deductible, charge incl. taxes should be input in "charge excl. taxes" field and 0 should be input in "VAT" field  
Expense can only be added if expense report is already saved as draft  


[ ] ==**ADD BUSINESS-RELATED TRAVEL EXPENSE (company)**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to add to. User is redirected to specific expense report HCI and clicks add button
2.	User completes form with appropriate data
3.	User confirms or cancels expense addition

- Form data to be completed: date*, type*, provision, origin*, destination*, distance*  

(* = required data for form validation)  

Expense can only be added if expense report is already saved as draft.


[ ] ==**EDIT BUSINESS-RELATED TRAVEL EXPENSE (company)**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to edit from. User is redirected to specific expense report HCI and clicks specific edit button
2.	User completes form with appropriate data (fields have current values as default values)
3.	User confirms or cancels expense addition

- Form data to be completed: date*, type*, provision, origin*, destination*, distance*  

(* = required data for form validation)  

Expense can only be added if expense report is already saved as draft.


[ ] ==**DELETE EXPENSE**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to delete expense from. User is redirected to specific expense report HCI and clicks specific delete button
2.	User is prompted for deletion confirmation as safety measure
3.	User confirms or cancels deletion

An expense can only be delete if expense report is already saved as draft.


[ ] ==**SAVE EXPENSE REPORT**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to save. User is redirected to specific expense and clicks save button
2.	User completes form with appropriate data (comments that are optional)
3.	User confirms or cancels

Only draft expense reports can be saved  
Comments will be displayed in dashboard HCI from then on  


[ ] ==**DELETE EXPENSE REPORT**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to delete. User is redirected to specific expense report HCI and clicks delete button
2.	User is prompted for deletion confirmation as safety measure
3.	User confirms or cancels

Only draft expense reports can be deleted  
If deleted expense report was awaiting validation, it is returned to draft status and the validation process is automatically cancelled


[ ] ==**SUBMIT EXPENSE REPORT FOR VALIDATION**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to submit for validation. User is redirected to specific expense report HCI and clicks validate button
2.	User completes form with appropriate data (comment that is optional)
3.	User confirms or cancels

User cannot (un)validate their own expense report; only the admin can  
Expense reports might have another status but those are not accessible on the user's end

----------

## STEP 2
[ ] ==**ATTACH SUPPORTING DOCUMENT TO EXPENSE**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to attach a document in. User is redirected to specific expense report HCI, browses supporting document files and clicks attach button
2.	User completes form with appropriate data
3.	User confirms or cancels attachment

- Form data to be completed: document type*, local image file*, description*  

(* = required data for form validation)  

Default description is image file name  
Supporting documents can be joined regardless of expense report status


[ ] ==**DELETE SUPPORTING DOCUMENT FROM EXPENSE**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to delete a document in. User is redirected to specific expense report HCI, browses supporting document files and clicks file link
2.	File information is displayed on click (description, file name, file size, upload date, last edit date)
3.	User clicks delete button
4.	User is prompted for deletion confirmation as safety measure
5.	User confirms or cancels deletion

Supporting documents can be deleted regardless of expense report status


[ ] ==**EDIT SUPPORTING DOCUMENT FROM EXPENSE**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to edit a document in. User is redirected to specific expense report HCI, browses supporting document files and clicks file link
2.	File information is displayed
3.	User clicks edit button
4.	User completes form with appropriate data (fields have current values as default values, including file)
5.	User confirms or cancels edition

- Form data to be completed: document type*, local image file*, description*  

(* = required data for form validation)  

Default description is image file name  
Supporting documents can be edited regardless of expense report status


[ ] ==**GENERATE EXPENSE WITH OCR**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to generate expense in. User is redirected to specific expense report HCI and clicks generate button
2.	User selects snapshot from gallery or takes snapshot on the spot
3.	OCR system parses data from snapshot and automatically compute rates
4.	Form loads with parsed data as default value in appropriate fields
5.	User may edit form data if necessary, then confirms or cancels


[ ] ==**ADD CUSTOMER INTERACTION-RELATED EXPENSE**==

Baseline scenario:
1.	Active user goes to HCI and selects and expense report to add to. User is redirected to specific expense report HCI and clicks add button
2.	User can complete overhead form with appropriate data 
3.	User can complete line charge form with appropriate data
4.	User confirms or cancels expense addition

- Overhead form data to be completed: date, category, type*, description, charge incl. and excl. taxes*
- Line charge form data to be completed: date*, type*, charge incl. and excl. taxes*  

(* = required data for form validation)  

If multiple VAT rates need to be provided, total sum should be input  
If VAT is not deductible, charge incl. taxes should be input in "charge excl. taxes" field and 0 should be input in "VAT" field  
Expense can only be added if expense report is already saved as draft  


[ ] ==**EDIT CUSTOMER INTERACTION-RELATED EXPENSE**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to edit from. User is redirected to specific expense report HCI and clicks specific edit button
2.	User can complete overhead form with appropriate data (fields have current values as default values)
3.	User can complete line charge form with appropriate data (fields have current values as default values)
4.	User confirms or cancels expense edition

- Overhead form data to be completed: date, category, type*, description, charge incl. and excl. taxes*
- Line charge form data to be completed: date*, type*, charge incl. and excl. taxes*  

(* = required data for form validation)  

If multiple VAT rates need to be provided, total sum should be input  
If VAT is not deductible, charge incl. taxes should be input in "charge excl. taxes" field and 0 should be input in "VAT" field  
Expense can only be added if expense report is already saved as draft  


[ ] ==**ADD CUSTOMER INTERACTION-RELATED TRAVEL EXPENSE**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to add to. User is redirected to specific expense report HCI and clicks add button
2.	User completes form with appropriate data
3.	User confirms or cancels expense addition

- Form data to be completed: date*, type*, provision, origin*, destination*, distance*, customer

(* = required data for form validation)  

Expense can only be added if expense report is already saved as draft


[ ] ==**EDIT CUSTOMER INTERACTION-RELATED TRAVEL EXPENSE**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to edit from. User is redirected to specific expense report HCI and clicks specific edit button
2.	User completes form with appropriate data (fields have current values as default values)
3.	User confirms or cancels expense edition

- Form data to be completed: date*, type*, provision, origin*, destination*, distance*, customer  

(* = required data for form validation)  

Expense can only be added if expense report is already saved as draft


[ ] ==**COPY EXPENSE**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to copy expense from. User is redirected to specific expense report HCI and clicks specific copy button
2.	User specifies to which expense report the expense needs to be copied. Only expense reports already saved as drafts can be selected. Default value is current expense report
3.	User confirms or cancels copy

When user specifies in which expense report the expense needs to be copied, charge incl. and excl. VAT are displayed for clarity  
An expense can only be copied if expense report is already saved as draft


[ ] ==**COPY EXPENSE REPORT**==

Baseline scenario:
1.	Active user goes to HCI and selects expense report to copy. User is redirected to specific expense report HCI and clicks copy button
2.	User specifies month and year
3.	User confirms or cancels

Expense reports can be copied regardless of status, and copies will automatically be saved as drafts